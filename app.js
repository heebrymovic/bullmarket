require("dotenv").config();
require("./config/database");

const express = require("express");
const app = express();

const path = require("path");
const cookieParser = require("cookie-parser");

const port = process.env.PORT || 3000;

// register view engine
app.set("view engine", "ejs");

app.set("views", path.join(__dirname, "views", "pages"));

// middleware
app.use(express.json());
app.use(cookieParser());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

app.get("/", (req, res) => {
  res.render(__dirname + "/views/index");
});

app.get("/about", (req, res) => {
  res.render(__dirname + "/views/pages/about");
});

app.get("/plan", (req, res) => {
  res.render(__dirname + "/views/pages/plan");
});

app.get("/blog", (req, res) => {
  res.render(__dirname + "/views/pages/blog");
});

app.get("/blog-details", (req, res) => {
  res.render(__dirname + "/views/pages/blog-details");
});

app.get("/error-404", (req, res) => {
  res.render(__dirname + "/views/pages/error-404");
});

app.get("/contact", (req, res) => {
  res.render(__dirname + "/views/pages/contact");
});

app.get("/dashboard", (req, res) => {
  res.render(__dirname + "/views/pages/dashboard");
});

app.get("/login", (req, res) => {
  res.render(__dirname + "/views/pages/login");
});

app.get("/registration", (req, res) => {
    res.render(__dirname + "/views/pages/registration");
  });
